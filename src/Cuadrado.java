
import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;


public class Cuadrado extends Figura{
    Point punt1;
    Point punt2;
    Point punt3;
    Point punt4;
    Cuadrado(Point p1, Point p2,Point p3, Point p4, Color _color){
    punt1=new Point(p1.x,p1.y);
    punt2=new Point(p2.x,p2.y);
    punt3=new Point(p3.x,p3.y);
    punt4=new Point(p4.x,p4.y);
    color=_color;
    }
}
